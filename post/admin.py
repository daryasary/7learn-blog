from django.contrib import admin
from django.contrib.admin import register

from post.models import Post


@register(Post)
class PostAdmin(admin.ModelAdmin):
    list_display = ('id','title', 'author_name', 'status', 'jalali_created_time')
    list_filter = ('status', 'author')
    search_fields = ('title', 'author__user__username')
    list_display_links = ('id', 'title', 'status',)
    readonly_fields = ('status', )
    actions = ('make_publish',)

    def author_name(self, obj):
        return obj.author.user.username
    author_name.short_description = "author"

    def save_model(self, request, obj, form, change):
        obj.author = request.user.author
        obj.save()

    def get_readonly_fields(self, request, obj=None):
        if request.user.is_superuser:
            return ()
        return self.readonly_fields

    def make_publish(self, request, queryset):
        queryset.update(status=Post.PUBLISHED)
        return queryset

    def get_queryset(self, request):
        queryset = super().get_queryset(request)
        if request.user.is_superuser:
            return queryset
        return queryset.filter(author=request.user.author)
