from django.contrib.auth.decorators import login_required
from django.http import HttpResponse
from django.shortcuts import render
from post.models import Post
from category.models import Category


@login_required
def post_list(request):
    cat1 = Category.objects.first()
    cat3 = Category.objects.create(name='Economic')
    cat4 = Category.objects.create(name='Politic')

    post4 = Post.objects.create(title="Post #4", author=request.user.author)
    post4.categories.add([cat1, cat3, cat4])

    # queryset = Post.objects.all().order_by('-created_time').first()
    # queryset = Post.objects.all().query
    # qs = queryset.filter(status=Post.ARCHIVED)
    # print(qs)
    # qs.delete()
    #
    # Post.objects.filter(status=Post.DRAFT, title='Post #1')
    #
    # Post.objects.all().values("title", "status")  # List of Dictionaries
    # Post.objects.all().values_list("title", "status")  # List of Sets
    # Post.objects.all().values_list("title")  # List of Sets
    # Post.objects.all().values_list("title", flat=True)  # List of values


    # post = Post.objects.get(id=1)
    # post.title = 'Post #1 modified'
    # post.status = Post.PUBLISHED
    # post.save()
    #
    # Post.objects.count()
    # Post.objects.all().delete()

    # category = Category.objects.get_or_create(id=1, defaults={'name': "Economic"})
    # category = Category.objects.create_or_update(id=1, defaults={'name': "Economic"})

    posts = Post.objects.select_related('author') \
        .prefetch_related("categories", "author__user")\
        .filter(author=request.user.author)
    titles = posts.values_list('title', flat=True)

    return HttpResponse(
        'Post count: {}\n{}'.format(posts.count(), '\n'.join(titles))
    )


def home_page(request):
    context = dict()
    context['posts_count'] = Post.objects.count()
    context['posts_list'] = Post.objects.all()
    return render(request, 'blog/home.html', context=context)


@login_required()
def post_detail(request, pk):
    post = Post.objects.filter(pk=pk).first()
    context = dict(post=post)
    return render(request, 'blog/detail.html', context=context)